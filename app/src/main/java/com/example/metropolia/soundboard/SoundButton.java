package com.example.metropolia.soundboard;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.net.Uri;
import android.widget.Toast;

import java.util.function.Supplier;

public class SoundButton extends android.support.v7.widget.AppCompatButton {
    MediaPlayer mp;

    public SoundButton(Context context){
        super(context);

    }

    public SoundButton(Context context, String text, int soundFile, Supplier<PlaybackParams> pbpSupplier){
        super(context);
        super.setText(text);
        mp = MediaPlayer.create(context, soundFile);
        this.setOnClickListener(
                view -> {
                    mp.stop();
                    try {
                        mp.prepare();
                    }catch (Exception e){
                        Toast.makeText(context, "Something terrible happened!", Toast.LENGTH_SHORT).show();
                    }
                    mp.setPlaybackParams(pbpSupplier.get());
                    mp.start();
                }
        );
    }

}
