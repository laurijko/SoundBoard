package com.example.metropolia.soundboard;

import android.media.PlaybackParams;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import java.util.Arrays;
import java.util.function.Supplier;

public class SoundBoard extends AppCompatActivity {

    PlaybackParams pbp;
    SeekBar sbPitch;
    SeekBar sbSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_board);

        LinearLayout layout = findViewById(R.id.layout);
        layout.setPadding(20, 20, 20, 20);

        pbp = new PlaybackParams();

        // Pitch slider
        TextView pitchText = new TextView(this);
        pitchText.setText("Pitch");

        sbPitch = new SeekBar(this);
        sbPitch.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        sbPitch.setProgress(50);

        pbp.setPitch(sbPitch.getProgress() / 50f);

        sbPitch.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float value = (i / 50f);
                if (value < 0.2f) value += 0.2f - value;
                pbp.setPitch(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // Speed slider
        TextView speedText = new TextView(this);
        speedText.setText("Speed");

        sbSpeed = new SeekBar(this);
        sbSpeed.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        sbSpeed.setProgress(50);

        pbp.setSpeed(sbSpeed.getProgress() / 50f);

        sbSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float value = (i / 50f);
                if (value < 0.2f) value += 0.2f - value;
                pbp.setSpeed(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        Button sResetBtn = new Button(this);
        sResetBtn.setText("Reset sliders");
        sResetBtn.setOnClickListener(view -> {
            sbPitch.setProgress(50);
            sbSpeed.setProgress(50);
        });

        FlexboxLayout sl = new FlexboxLayout(this);
        sl.setFlexDirection(FlexDirection.COLUMN);

        sl.addView(pitchText);
        sl.addView(sbPitch);
        sl.addView(speedText);
        sl.addView(sbSpeed);
        sl.addView(sResetBtn);

        layout.addView(sl);

        FlexboxLayout btnl = new FlexboxLayout(this);

        btnl.setLayoutParams(new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT));

        btnl.setFlexWrap(FlexWrap.WRAP);

        Supplier<PlaybackParams> pbpSupplier = () -> pbp;

        Arrays.asList(
            new SoundButton(this, "Blop", R.raw.late1, pbpSupplier),
            new SoundButton(this, "Tweet", R.raw.late2, pbpSupplier),
            new SoundButton(this, "Otson oksennus", R.raw.otso1, pbpSupplier),
            new SoundButton(this, "Quack", R.raw.late3, pbpSupplier),
            new SoundButton(this, "Clap", R.raw.late4, pbpSupplier),
            new SoundButton(this, "Garden", R.raw.late5, pbpSupplier),
            new SoundButton(this, "AAAH", R.raw.yoshi_aaa, pbpSupplier),
            new SoundButton(this, "OWOWOW", R.raw.yoshi_awawawa, pbpSupplier),
            new SoundButton(this, "tongue", R.raw.yoshi_tongue, pbpSupplier),
            new SoundButton(this, "Pom", R.raw.yoshi_pom, pbpSupplier),
            new SoundButton(this, "Yahoo", R.raw.yoshi_yahoo, pbpSupplier),
            new SoundButton(this, "Yoshii", R.raw.yoshi_yoshii, pbpSupplier),
            new SoundButton(this, "Haa", R.raw.yoshi_haa, pbpSupplier),
            new SoundButton(this, "Otson huuto", R.raw.otso2, pbpSupplier),
            new SoundButton(this, "Power Overwhelming", R.raw.sc2_power, pbpSupplier)
        ).forEach(b -> btnl.addView(b));

        layout.addView(btnl);
    }
}
